<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>{$name}</title>
{$css}<link rel="shortcut icon" href="{$favicon}">
</head>
<body>
<h1>{$name}</h1>
{$slogan}<div class="menu">
{$linkbar}
</div>
{$content}
</body>
</html>